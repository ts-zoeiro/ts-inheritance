import {Report} from "./Report";
import { TabbedReport } from "./TabbedReport";
console.log("Class:")
let report: Report = new Report(["Line 0", "Line 1", "Line 3", "Line 4"]);
report.run();

console.log("\nInheritance: ")
var headers: string[] = ["Name"];
var data: string[] = ["Alice", "Noel", "Paul", "Louis"];
var r : TabbedReport = new TabbedReport(headers, data);
r.run();


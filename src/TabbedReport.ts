import { Report } from "./Report";

export class TabbedReport extends Report{
    headers: Array<string>;

    constructor(headers: string[], values: string[]){
        super(values);
        this.headers = headers;
    }

    run () {
        console.log("headers", this.headers);
        super.run();
    }
}